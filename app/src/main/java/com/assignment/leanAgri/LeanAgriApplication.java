package com.assignment.leanAgri;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.assignment.leanAgri.injection.component.ApplicationComponent;
import com.assignment.leanAgri.injection.component.DaggerApplicationComponent;
import com.assignment.leanAgri.injection.module.ApplicationModule;
import com.assignment.leanAgri.injection.module.NetworkModule;

public class LeanAgriApplication extends Application {
    ApplicationComponent mApplicationComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule(this))
                .build();


    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }
    public static LeanAgriApplication get(Context context) {
        return (LeanAgriApplication) context.getApplicationContext();
    }

}
