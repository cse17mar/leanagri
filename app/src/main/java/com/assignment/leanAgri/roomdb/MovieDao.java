package com.assignment.leanAgri.roomdb;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MovieDao {
    @Insert
    void addAllMovies(List<MovieData> movieDataList);

    @Query("SELECT * FROM movieData")
    List<MovieData> getMovieList();

    @Query("SELECT * FROM movieData ORDER BY vote_count DESC")
    List<MovieData> getMovieByVoteCount();

    @Query("SELECT * FROM movieData ORDER BY vote_count ASC")
    List<MovieData> getMovieByVoteCountDes();

    @Query("SELECT * FROM movieData ORDER BY title")
    List<MovieData> getMovieByName();

    @Query("SELECT * FROM movieData ORDER BY release_date")
    List<MovieData> getMovieByReleaseDate();

    @Query("DELETE FROM movieData")
    void clearData();

}




