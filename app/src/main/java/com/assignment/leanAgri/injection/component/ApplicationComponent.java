package com.assignment.leanAgri.injection.component;

import android.app.Application;
import android.content.Context;

import com.assignment.leanAgri.LeanAgriApplication;
import com.assignment.leanAgri.data.AppDataManager;
import com.assignment.leanAgri.injection.annotation.ApplicationContext;
import com.assignment.leanAgri.injection.module.ApplicationModule;
import com.assignment.leanAgri.injection.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void inject(LeanAgriApplication leanAgriApplication);

    @ApplicationContext
    Context context();

    Application application();


    AppDataManager dataManager();

}
