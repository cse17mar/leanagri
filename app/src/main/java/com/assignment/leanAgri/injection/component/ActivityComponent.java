package com.assignment.leanAgri.injection.component;


import com.assignment.leanAgri.injection.annotation.PerActivity;
import com.assignment.leanAgri.injection.module.ActivityModule;
import com.assignment.leanAgri.ui.movie.MovieFragment;

import dagger.Component;


@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    // todo activity injection




    // todo fragment injection
    void inject(MovieFragment movieFragment);





}
