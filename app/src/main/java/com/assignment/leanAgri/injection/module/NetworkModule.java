package com.assignment.leanAgri.injection.module;

import android.app.Application;

import com.assignment.leanAgri.data.api.LeanAgriApiHelper;
import com.assignment.leanAgri.utils.AppConstant;


import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class NetworkModule {

    private final Application mApplication;

    private static final long CONNECT_TIMEOUT = 15;
    private static final long WRITE_TIMEOUT =15 ;
    private static final long TIMEOUT = 15;

    public NetworkModule(Application mApplication){this.mApplication=mApplication;}

    @Provides
    @Singleton
    LeanAgriApiHelper provideGoogleApi() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS);

        builder.addInterceptor(loggingInterceptor);

        return new Retrofit.Builder()
                .baseUrl(AppConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(builder.build())
                .build().create(LeanAgriApiHelper.class);

    }



}
