package com.assignment.leanAgri.utils;

public interface AppConstant {
    String BASE_URL = "https://api.themoviedb.org/3/";
    int RES_OK = 200;

    //key constant
    String OPEN_VIEW ="OpenFragmentView";
    String DATA_MOVIE_DETAILS ="DATA_MOVIE_DETAILS";
    // value constant
    String MOVIE_VIEW ="OpenMovieFragment";
    String MOVIE_DETAILAS_VIEW ="OpenMovieDetailsFragment";

    // api constant
    String API_KEY = "17eef85cc72a2797ea5a8382acc477f8";
    String LANGUAGE = "en-US";
    String REGION_FOR = "IN";
    String POSTER_BASE_URL = "http://image.tmdb.org/t/p/w185";
}
