package com.assignment.leanAgri.data;

import android.content.Context;


import com.assignment.leanAgri.data.api.LeanAgriApiHelper;
import com.assignment.leanAgri.injection.annotation.ApplicationContext;

import javax.inject.Inject;

import dagger.Module;

@Module
public class AppDataManager {
    private static final String TAG = "AppDataManager";

    private final LeanAgriApiHelper leanAgriApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context, LeanAgriApiHelper apiHelper) {
        leanAgriApiHelper = apiHelper;
    }

    public LeanAgriApiHelper getApiHelper() {
        return leanAgriApiHelper;
    }
}
