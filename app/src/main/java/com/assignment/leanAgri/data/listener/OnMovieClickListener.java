package com.assignment.leanAgri.data.listener;

public interface OnMovieClickListener {
    void onMovieClick(int clickedPosition);
}
