package com.assignment.leanAgri.data.api;


import com.assignment.leanAgri.data.model.MovieResponse;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LeanAgriApiHelper {
    @GET("movie/now_playing")
    Single<MovieResponse> getMoviesData(@Query("page") int page, @Query("api_key") String apiKey, @Query("language") String language, @Query("region") String region);

}
