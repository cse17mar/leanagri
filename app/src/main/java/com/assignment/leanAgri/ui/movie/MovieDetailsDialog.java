package com.assignment.leanAgri.ui.movie;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.assignment.leanAgri.R;
import com.assignment.leanAgri.roomdb.MovieData;
import com.assignment.leanAgri.utils.Util;
import com.google.gson.Gson;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.assignment.leanAgri.utils.AppConstant.DATA_MOVIE_DETAILS;
import static com.assignment.leanAgri.utils.AppConstant.POSTER_BASE_URL;

public class MovieDetailsDialog extends BottomSheetDialogFragment {
    @BindView(R.id.ivPoster)
    AppCompatImageView ivPoster;
    @BindView(R.id.tvMovieName)
    AppCompatTextView tvMovieName;
    @BindView(R.id.tvOrinalLag)
    AppCompatTextView tvOrinalLag;
    @BindView(R.id.tvMovieReleaseDate)
    AppCompatTextView tvMovieReleaseDate;
    @BindView(R.id.rbMovieRating)
    AppCompatRatingBar rbMovieRating;
    @BindView(R.id.tvOverDes)
    AppCompatTextView tvOverDes;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.cbAdult)
    RadioButton cbAdult;
    private View layoutView;
    private Dialog dialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layoutView = inflater.inflate(R.layout.dialog_movie_detials_layout, container, false);
        ButterKnife.bind(this, layoutView);
        initView();
        return layoutView;
    }

    private void initView() {
        MovieData movieData = new Gson().fromJson(getArguments().getString(DATA_MOVIE_DETAILS, null), MovieData.class);
        if (movieData != null) {
            // we can set Data Direct also ==> ((AppCompatTextView)layoutView.findViewById(R.id.tvTitle)).setText("set text");
            tvTitle.setText(movieData.getTitle());
            tvOrinalLag.setText(movieData.getOriginalLanguage());
            cbAdult.setChecked(movieData.getAdult());
            tvOverDes.setText(movieData.getOverview());
            tvMovieName.setText(movieData.getOriginalTitle());
            rbMovieRating.setRating(movieData.getVoteAverage() / 2);   // ratted out 10 , but we are showing out of 5
            Log.d("Rating====>", "" + rbMovieRating.getRating());
            tvMovieReleaseDate.setText(movieData.getReleaseDate());
            String fullPosterUrlPath = POSTER_BASE_URL + movieData.getPosterPath();
            Log.d("path", fullPosterUrlPath);
            if (Util.hasInternet(getContext())) {
                Picasso picasso = Picasso.with(getContext());
                picasso.setLoggingEnabled(true);
                picasso.load(fullPosterUrlPath)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .error(R.drawable.ic_place_holder)
                        .into(ivPoster);
            }
        }else {

        }
    }

    @OnClick(R.id.ivCancel)
    void dismissDialog() {
        dialog.dismiss();
    }

}