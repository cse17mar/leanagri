package com.assignment.leanAgri.ui.movie;

import com.assignment.leanAgri.data.model.MovieResponse;
import com.assignment.leanAgri.ui.base.BaseMVPView;

public interface MovieMVPView extends BaseMVPView {
    void onResponseMovieData(MovieResponse movieResponse);
    void offlineData();
    void openFilter();
    void errorDialog();
}
