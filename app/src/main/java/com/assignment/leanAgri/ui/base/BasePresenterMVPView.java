package com.assignment.leanAgri.ui.base;

public interface BasePresenterMVPView<V extends BaseMVPView>{
    void attachView(V mvpView);

    void detachView();
}
