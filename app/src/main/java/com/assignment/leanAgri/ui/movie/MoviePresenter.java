package com.assignment.leanAgri.ui.movie;

import com.assignment.leanAgri.R;
import com.assignment.leanAgri.data.AppDataManager;
import com.assignment.leanAgri.data.model.MovieResponse;
import com.assignment.leanAgri.rx.SchedulerProvider;
import com.assignment.leanAgri.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

import static com.assignment.leanAgri.utils.AppConstant.API_KEY;
import static com.assignment.leanAgri.utils.AppConstant.LANGUAGE;
import static com.assignment.leanAgri.utils.AppConstant.REGION_FOR;

public class MoviePresenter<V extends MovieMVPView> extends BasePresenter<V> implements MoviePresenterMVPView<V> {
    @Inject
    public MoviePresenter(AppDataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void getMovie(int page) {
        if (getMvpView().hasInternetConnection()) {
            getMvpView().showLoading();
            getCompositeDisposable().add(getAppDataManager()
                    .getApiHelper()
                    .getMoviesData(page, API_KEY, LANGUAGE, REGION_FOR)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribeWith(new DisposableSingleObserver<MovieResponse>() {
                        @Override
                        public void onSuccess(MovieResponse movieResponse) {
                            getMvpView().hideLoading();
                            getMvpView().onResponseMovieData(movieResponse);
                        }

                        @Override
                        public void onError(Throwable e) {
                            getMvpView().hideLoading();
                            getMvpView().onError(e);
                            getMvpView().errorDialog();

                        }
                    }));
        } else {
            getMvpView().offlineData();
        }
    }

    @Override
    public void filterClicked() {
    getMvpView().openFilter();
    }
}

