package com.assignment.leanAgri.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.Toast;

import com.assignment.leanAgri.LeanAgriApplication;
import com.assignment.leanAgri.R;
import com.assignment.leanAgri.injection.component.ActivityComponent;
import com.assignment.leanAgri.injection.component.DaggerActivityComponent;
import com.assignment.leanAgri.injection.module.ActivityModule;
import com.assignment.leanAgri.utils.Util;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements BaseMVPView{
    private ActivityComponent mActivityComponent;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentResource());
        ButterKnife.bind(this);
        init(savedInstanceState);

    }

    public ActivityComponent getActivityComponent() {
        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((LeanAgriApplication) getApplication()).getComponent())
                .build();
        return mActivityComponent;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @LayoutRes
    protected abstract int getContentResource();

    protected abstract void init(@Nullable Bundle state);

    public void addFragment(final Fragment fragment, final String tag, final boolean isAdd) {
        try {
            if (!isFinishing()) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        if (!fragmentManager.isStateSaved() && fragmentManager.findFragmentByTag(tag) == null) {
                            if (isAdd) {
                                fragmentTransaction.add(R.id.rootContainer, fragment, tag);
                                fragmentTransaction.addToBackStack(tag);
                            } else {
                                fragmentTransaction.replace(R.id.rootContainer, fragment, tag);
                            }
                            fragmentTransaction.commitAllowingStateLoss();
                            fragmentManager.executePendingTransactions();
                        } else {
                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        }
                    }
                });
            }
        } catch (Exception ignored) {

        }
    }

    @Override
    public void showLoading() {
        hideLoading();
        progressDialog = Util.showLoadingDialog(this);

    }

    @Override
    public void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }
    @Override
    public void showMessage(String message) {
        if (message != null) {
            Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 60);
            toast.show();
        } else {
            Toast toast = Toast.makeText(this, getString(R.string.some_error), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 60);
            toast.show();
        }
    }

    @Override
    public void showMessage(@StringRes int resId) {
        showMessage(getString(resId));
    }
    @Override
    public void onError(Throwable throwable) {
        hideLoading();
    }
    @Override
    public boolean hasInternetConnection() {
        boolean hasInternetConnection = false;
        ConnectivityManager mCM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = mCM.getActiveNetworkInfo();
        hasInternetConnection = netInfo != null && netInfo.isConnectedOrConnecting();
        if (!hasInternetConnection) {
            showMessage(R.string.no_internet_conn);
        }
        return hasInternetConnection;

    }


}
