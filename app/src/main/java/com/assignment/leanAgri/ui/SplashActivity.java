package com.assignment.leanAgri.ui;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.assignment.leanAgri.R;
import com.assignment.leanAgri.ui.ActionActivity;
import com.assignment.leanAgri.ui.base.BaseActivity;

import static com.assignment.leanAgri.utils.AppConstant.MOVIE_VIEW;
import static com.assignment.leanAgri.utils.AppConstant.OPEN_VIEW;

public class SplashActivity extends BaseActivity {
    private long SPLASH_DISPLAY_TIME =2000;


    @Override
    protected int getContentResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void init(@Nullable Bundle state) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent launchIntent = ActionActivity.getStartIntent(getApplicationContext());
                launchIntent.putExtra(OPEN_VIEW, MOVIE_VIEW);
                startActivity(launchIntent);
                finish();
            }
        }, SPLASH_DISPLAY_TIME);

    }


}
