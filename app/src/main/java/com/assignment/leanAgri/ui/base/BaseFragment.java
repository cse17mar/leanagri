package com.assignment.leanAgri.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.assignment.leanAgri.R;
import com.assignment.leanAgri.utils.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public abstract class BaseFragment extends Fragment implements BaseMVPView {
    public static final String ARG_PARAM = "ARG_PARAM";
    private View layoutView;
    private ProgressDialog progressDialog;
    @Nullable
    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            layoutView = inflater.inflate(getContentResource(), container, false);
            ButterKnife.bind(this, layoutView);
        } catch (InflateException ex) {
            ex.printStackTrace();
        }
        init(savedInstanceState, layoutView);

        return layoutView;
    }

    protected abstract int getContentResource();

    protected abstract void init(Bundle savedInstanceState, View layoutView);

    @Override
    public void showLoading() {
        hideLoading();
        progressDialog = Util.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    @Optional
    @Nullable
    @OnClick(R.id.ivBack)
    void backPress() {
        getActivity().onBackPressed();
    }

    @Override
    public void showMessage(String message) {
        if (message != null) {
            Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 60);
            toast.show();
        } else {
            Toast toast = Toast.makeText(getContext(), getString(R.string.some_error), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 60);
            toast.show();
        }
    }

    @Override
    public void showMessage(@StringRes int resId) {
        showMessage(getString(resId));
    }



    @Override
    public boolean hasInternetConnection() {
        boolean hasInternetConnection = false;
        ConnectivityManager mCM = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = mCM.getActiveNetworkInfo();
        hasInternetConnection = netInfo != null && netInfo.isConnectedOrConnecting();
        if (!hasInternetConnection) {
            showMessage(R.string.no_internet_conn);
        }
        return hasInternetConnection;

    }
    @Override
    public void onError(Throwable throwable) {
        hideLoading();
    }
}
