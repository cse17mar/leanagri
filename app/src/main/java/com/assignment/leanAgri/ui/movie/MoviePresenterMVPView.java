package com.assignment.leanAgri.ui.movie;

public interface MoviePresenterMVPView<V extends MovieMVPView> {
    void getMovie(int page);
    void filterClicked();
}
