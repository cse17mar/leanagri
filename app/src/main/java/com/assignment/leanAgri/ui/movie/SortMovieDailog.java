package com.assignment.leanAgri.ui.movie;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.assignment.leanAgri.R;
import com.assignment.leanAgri.data.listener.OnSortSelectedLisener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SortMovieDailog extends BottomSheetDialogFragment {
    @BindView(R.id.rgMovie)
    RadioGroup rgMovieSort;
    private View layoutView;
    private Dialog dialog;
    private OnSortSelectedLisener onSortSelectedLisener;
    private int id;
    private int selectedId;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layoutView = inflater.inflate(R.layout.dialog_movie_sort_layout, container, false);
        ButterKnife.bind(this, layoutView);
        initView();
        return layoutView;
    }

    private void initView() {
        rgMovieSort.check(selectedId);
        rgMovieSort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                onSortSelectedLisener.onSortIdSelected(checkedId);
            }
        });
    }

    public void sendListener(OnSortSelectedLisener onSortSelectedLisener, int selectedId) {
        this.onSortSelectedLisener = onSortSelectedLisener;
        this.selectedId = selectedId;
    }
}