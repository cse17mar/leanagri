package com.assignment.leanAgri.ui.movie;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.assignment.leanAgri.R;
import com.assignment.leanAgri.data.listener.OnMovieClickListener;
import com.assignment.leanAgri.roomdb.MovieData;
import com.assignment.leanAgri.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.internal.Utils;

import static com.assignment.leanAgri.utils.AppConstant.POSTER_BASE_URL;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    private List<MovieData> movieDataList;
    private Context context;
    private OnMovieClickListener listener;

    public MovieAdapter(List<MovieData> movieDataList, OnMovieClickListener listener) {
        this.listener = listener;
        this.movieDataList = movieDataList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemViem = LayoutInflater.from(context).inflate(R.layout.item_movie, parent, false);
        return new ViewHolder(itemViem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onMovieClick(position);
            }
        });
        holder.tvOrinalLag.setText(movieDataList.get(position).getOriginalLanguage());
        holder.tvMovieName.setText(movieDataList.get(position).getTitle());
        holder.rbMovieRating.setRating(movieDataList.get(position).getVoteAverage() / 2); // ratted out 10 , but we are showing out of 5
        holder.tvMovieReleaseDate.setText(movieDataList.get(position).getReleaseDate());
        String fullPosterUrlPath = POSTER_BASE_URL + movieDataList.get(position).getPosterPath();
        if (Util.hasInternet(context)) {
            Picasso.with(context)
                    .load(fullPosterUrlPath)
                    .into(holder.ivPoster);

        } else {

        }
    }

    @Override
    public int getItemCount() {
        return movieDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        @BindView(R.id.ivPoster)
        AppCompatImageView ivPoster;

        @BindView(R.id.tvMovieName)
        AppCompatTextView tvMovieName;

        @BindView(R.id.tvMovieReleaseDate)
        AppCompatTextView tvMovieReleaseDate;

        @BindView(R.id.rbMovieRating)
        AppCompatRatingBar rbMovieRating;

        @BindView(R.id.tvOrinalLag)
        AppCompatTextView tvOrinalLag;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this,itemView);
        }
    }
}