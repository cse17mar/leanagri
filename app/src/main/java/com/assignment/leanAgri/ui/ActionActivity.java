package com.assignment.leanAgri.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.assignment.leanAgri.R;
import com.assignment.leanAgri.ui.base.BaseActivity;
import com.assignment.leanAgri.ui.movie.MovieFragment;

import static com.assignment.leanAgri.utils.AppConstant.OPEN_VIEW;

public class ActionActivity extends BaseActivity {
    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ActionActivity.class);
        return intent;
    }
    @Override
    protected int getContentResource() {
        return R.layout.activity_action;
    }

    @Override
    protected void init(@Nullable Bundle state) {
        String openView = getIntent().getStringExtra(OPEN_VIEW);
        if(openView.equalsIgnoreCase(openView)){
            addFragment(MovieFragment.newInstance(new Bundle()),openView,false);
        }

    }
}
