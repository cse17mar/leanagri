package com.assignment.leanAgri.ui.base;

import android.support.annotation.StringRes;

public interface BaseMVPView {
    void showLoading();

    void hideLoading();

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean hasInternetConnection();

    void onError(Throwable throwable);

}
