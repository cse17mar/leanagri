package com.assignment.leanAgri.ui.movie;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.assignment.leanAgri.R;
import com.assignment.leanAgri.data.listener.OnMovieClickListener;
import com.assignment.leanAgri.data.listener.OnSortSelectedLisener;
import com.assignment.leanAgri.data.model.MovieResponse;
import com.assignment.leanAgri.roomdb.MovieData;
import com.assignment.leanAgri.roomdb.MovieDataBase;
import com.assignment.leanAgri.ui.base.BaseActivity;
import com.assignment.leanAgri.ui.base.BaseFragment;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.assignment.leanAgri.utils.AppConstant.DATA_MOVIE_DETAILS;

public class MovieFragment extends BaseFragment implements MovieMVPView, OnMovieClickListener, OnSortSelectedLisener {
    @BindView(R.id.recMovie)
    RecyclerView recMovie;

    @BindView(R.id.noItem)
    View noItem;
    @Inject
    MoviePresenter<MovieMVPView> mvpViewMoviePresenter;
    private boolean isPagination;
    private int nextPage = 1; // initially start page itselef is next page
    private List<MovieData> movieDataList = new ArrayList<>();
    private MovieDataBase movieDataBase;
    private MovieAdapter movieAdapter;
    private SortMovieDailog sortMovieDailog;
    private int selectedId = R.id.rbDefault; // bydefault

    public static Fragment newInstance(Bundle bundle) {
        MovieFragment fragment = new MovieFragment();
        Bundle args = new Bundle();
        args.putBundle(ARG_PARAM, bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentResource() {
        return R.layout.fragment_movie;
    }

    @Override
    protected void init(Bundle savedInstanceState, View layoutView) {
        movieDataBase = Room.databaseBuilder(getContext(), MovieDataBase.class, "movie_data_new").allowMainThreadQueries().build();
        ((BaseActivity) getActivity()).getActivityComponent().inject(this);
        mvpViewMoviePresenter.attachView(this);
        recMovie.setLayoutManager(new LinearLayoutManager(getContext()));
        movieAdapter = new MovieAdapter(movieDataList, this);
        recMovie.setAdapter(movieAdapter);
        mvpViewMoviePresenter.getMovie(nextPage);
        recyclerAddScroll();
    }

    private void recyclerAddScroll() {
        recMovie.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!recyclerView.canScrollVertically(1)) {
                    if (isPagination) {
                        mvpViewMoviePresenter.getMovie(nextPage);
                    }
                }
            }
        });
    }

    @Override
    public void onResponseMovieData(MovieResponse movieResponse) {
        // i api we are getting Total page ano and current page no ,then paggination requred up to current page< total page
        isPagination = movieResponse.getCurrentPage() < movieResponse.getTotalPages();
        // if you sroll next time then need to be send current page+1
        nextPage = movieResponse.getCurrentPage() + 1;
        movieDataList.addAll(movieResponse.getMovieDataList());
        movieDataBase.getMovieDao().clearData();
        movieDataBase.getMovieDao().addAllMovies(movieDataList);
        notifyAdapter();
    }

    private void notifyAdapter() {
        movieAdapter.notifyDataSetChanged();
        if (movieDataList.size() > 0) {
            noItem.setVisibility(View.GONE);
        } else {
            noItem.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void offlineData() {
        movieDataList.clear();
        movieDataList.addAll(movieDataBase.getMovieDao().getMovieList());
        notifyAdapter();
    }

    @Override
    public void openFilter() {
        sortMovieDailog = new SortMovieDailog();
        sortMovieDailog.sendListener(this, selectedId);
        sortMovieDailog.setCancelable(true);
        sortMovieDailog.show(getFragmentManager(), SortMovieDailog.class.getName());
    }

    @Override
    public void errorDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(
                getContext()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(getString(R.string.somthing_went_wrong));
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mvpViewMoviePresenter.getMovie(nextPage);

            }
        });

        alertDialog.show();
    }

    @Override
    public void onMovieClick(int clickedPosition) {
        recMovie.scrollToPosition(clickedPosition);
        Bundle bundle = new Bundle();
        bundle.putString(DATA_MOVIE_DETAILS, new Gson().toJson(movieDataList.get(clickedPosition)));
        MovieDetailsDialog movieDetailsDialog = new MovieDetailsDialog();
        movieDetailsDialog.setArguments(bundle);
        movieDetailsDialog.setCancelable(true);
        movieDetailsDialog.show(getFragmentManager(), MovieDetailsDialog.class.getName());


    }


    @OnClick(R.id.ivFilter)
    void filterClicked() {
        if (movieDataList.size() > 0) {
            mvpViewMoviePresenter.filterClicked();
        } else {
            showMessage(R.string.no_data);
        }
    }

    @Override
    public void onSortIdSelected(int checkId) {
        this.selectedId = checkId;
        movieDataList.clear();
        sortMovieDailog.dismiss();
        switch (selectedId) {
            case R.id.rbDefault:
                movieDataList.addAll(movieDataBase.getMovieDao().getMovieList());
                break;
            case R.id.rbreleasDate:
                movieDataList.addAll(movieDataBase.getMovieDao().getMovieByReleaseDate());
                break;
            case R.id.rbName:
                movieDataList.addAll(movieDataBase.getMovieDao().getMovieByName());
                break;
            case R.id.rbRatinghl:
                movieDataList.addAll(movieDataBase.getMovieDao().getMovieByVoteCount());
                break;
            case R.id.rbRatinglh:
                movieDataList.addAll(movieDataBase.getMovieDao().getMovieByVoteCountDes());
                break;
        }
        notifyAdapter();
        if (movieDataList.size() > 0) {
            recMovie.smoothScrollToPosition(0);
        }

    }
}
